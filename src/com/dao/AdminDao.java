package com.dao;

import java.util.List;

import com.model.Admin;
import com.model.VotersList;

public interface AdminDao {
	 public int adminAuthentication(Admin admin);

	  public List<VotersList> viewAll();

	  public int add(VotersList list);

	  public void update(VotersList list);

	  public void delete(VotersList list);

}
