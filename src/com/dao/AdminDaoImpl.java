package com.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.model.Admin;
import com.model.VotersList;
import com.util.Db;
import com.util.Query;

public class AdminDaoImpl implements AdminDao {
	int result;
	PreparedStatement pst;
	ResultSet rs;
	@Override
	public int adminAuthentication(Admin admin) {
		result = 0;
		try {
			pst = Db.getCon().prepareStatement(Query.adminLogin);
			pst.setString(1, admin.getUid());
			pst.setString(2, admin.getPassword());
			rs = pst.executeQuery();
			while (rs.next()) {
				result++;

			}

		} catch (ClassNotFoundException | SQLException e) {
			System.out.println("Exception oocurs in Admin Authentication");
			// e.printStackTrace();
		}
		return result;
	}

	@Override
	public List<VotersList> viewAll() {
		List<VotersList> list = new ArrayList<VotersList>();
		try {
			pst = Db.getCon().prepareStatement(Query.viewAll);
			rs = pst.executeQuery();
			while (rs.next()) {
	VotersList list1 = new VotersList(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4));
				list.add(list1);

			}
		} catch (ClassNotFoundException | SQLException e) {
			System.out.println("Exception occurs in view All Fruits");
		}
		return list;
	}

	@Override
	public int add(VotersList list) {
		try {
			pst = Db.getCon().prepareStatement(Query.add);
			pst.setInt(1, list.getVid());
			pst.setString(2, list.getVname());
			pst.setString(3, list.getVgender());
			pst.setString(4, list.getVaddress());
			result = pst.executeUpdate();

		} catch (ClassNotFoundException | SQLException e) {

		}

		return result;
	}

	@Override
	public void delete(VotersList list) {
		try {
			pst = Db.getCon().prepareStatement(Query.delete);
			pst.setInt(1, list.getVid());
			pst.executeUpdate();
		} catch (ClassNotFoundException | SQLException e) {

		}
	}

	@Override
	public void update(VotersList list) {
		try {
			pst = Db.getCon().prepareStatement(Query.update);
			pst.setInt(1, list.getVid());
			pst.setString(2, list.getVname());
			pst.setString(3, list.getVgender());
			pst.setString(4, list.getVaddress());
			pst.executeUpdate();
		} catch (ClassNotFoundException | SQLException e) {

		}

	}

}
