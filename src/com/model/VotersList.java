package com.model;

public class VotersList {
	private Integer vid;
	private String vname;
	private String vgender;
	private String vaddress;
	public VotersList() {
		// TODO Auto-generated constructor stub
	}
	public VotersList(Integer vid, String vname, String vgender, String vaddress) {
		super();
		this.vid = vid;
		this.vname = vname;
		this.vgender = vgender;
		this.vaddress = vaddress;
	}
	public Integer getVid() {
		return vid;
	}
	public void setVid(Integer vid) {
		this.vid = vid;
	}
	public String getVname() {
		return vname;
	}
	public void setVname(String vname) {
		this.vname = vname;
	}
	public String getVgender() {
		return vgender;
	}
	public void setVgender(String vgender) {
		this.vgender = vgender;
	}
	public String getVaddress() {
		return vaddress;
	}
	public void setVaddress(String vaddress) {
		this.vaddress = vaddress;
	}
	

}
