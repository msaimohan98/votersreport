package com.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.dao.AdminDao;
import com.dao.AdminDaoImpl;
import com.model.VotersList;

@WebServlet(name="Admincrudoperations", urlPatterns= {"/admincrudoperations","/insert","/update1"})
public class AdminCrudOperationsController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public AdminCrudOperationsController() {
		super();

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
	    String url = request.getServletPath();
	    AdminDao dao = new AdminDaoImpl();
	    VotersList list = new VotersList();

	    if (url.equals("/admincrudoperations")) {

	      List<VotersList> list1 = dao.viewAll();

	      request.setAttribute("votersList", list1);
	      request.getRequestDispatcher("admincrudoperations.jsp").forward(request, response);
	    } else if (url.equals("/home")) {
	      List<VotersList> list2 = dao.viewAll();
	      request.setAttribute("voterslist", list2);
	      request.getRequestDispatcher("admin.jsp").forward(request, response);
	    } else if (url.equals("/delete")) {
	      int vid = Integer.parseInt(request.getParameter("vid"));
	      list.setVid(vid);
	      dao.delete(list);
	      PrintWriter out = response.getWriter();
	      response.setContentType("text/html");
	      out.print(vid + " deleted successfully<br/>");
	      List<VotersList> List = dao.viewAll();
	      request.setAttribute("voterslist", List);
	      request.getRequestDispatcher("admincrudoperations.jsp").include(request, response);

	    } else if (url.equals("/insert")) {
	      request.getRequestDispatcher("insert.jsp").forward(request, response);
	    } else if (url.equals("/signout")) {
	      HttpSession session = request.getSession(false);
	      Integer uid = (Integer) session.getAttribute("uid");
	      System.out.println(uid + " logged out @ " + new Date());
	      session.invalidate();
	      request.setAttribute("uid", uid + "  Logged out successfully!!!");
	      request.getRequestDispatcher("logout.jsp").forward(request, response);
	    } else if (url.equals("/update1")) {
	    	int vid = Integer.parseInt(request.getParameter("vid"));
			String vname = request.getParameter("vname");
			String vgender = request.getParameter("vgender");
			String vaddress = request.getParameter("vaddress");
	      VotersList List = new VotersList(vid, vname,vgender,vaddress);
	      request.setAttribute("voterslist", List);
	      request.getRequestDispatcher("update.jsp").forward(request, response);
	    }

	  }

	}


