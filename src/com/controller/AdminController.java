package com.controller;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.dao.AdminDao;
import com.dao.AdminDaoImpl;
import com.model.Admin;
import com.model.VotersList;

@WebServlet(name="AdminController",urlPatterns = {"/login","/add","/update"})
public class AdminController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public AdminController() {
		super();

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String url = request.getServletPath();
		AdminDao dao = new AdminDaoImpl();

		if (url.equals("/login")) {
			String uid = request.getParameter("uid");
			String password = request.getParameter("password");

			HttpSession session = request.getSession(true);
			session.setAttribute("uid", uid);
			System.out.println(uid + "Logged in @" + new Date(session.getCreationTime()));

			Admin admin = new Admin(uid, password);
			int result = dao.adminAuthentication(admin);
			if (result > 0) {
				List<VotersList> list = dao.viewAll();
				request.setAttribute("voterslist", list);
				request.getRequestDispatcher("admin.jsp").forward(request, response);
			} else {
				request.setAttribute("error", "Hi" + uid + ", Please check your credentials");
				request.getRequestDispatcher("index.jsp").forward(request, response);
			}
		} else if (url.equals("/add")) {
			int vid = Integer.parseInt(request.getParameter("vid"));
			String vname = request.getParameter("vname");
			String vgender = request.getParameter("vgender");
			String vaddress = request.getParameter("vaddress");

			VotersList List = new VotersList(vid, vname, vgender, vaddress);
			int result = dao.add(List);
			if (result > 0) {
				request.setAttribute("msg", vid + "inserted succesfully");
			} else {
				request.setAttribute("msg", vid + "duplicate entry");
			}
			List<VotersList> list = dao.viewAll();
			request.setAttribute("voterslist", list);
			request.getRequestDispatcher("admincrudoperations.jsp").forward(request, response);
		} else if (url.equals("/update")) {
			int vid = Integer.parseInt(request.getParameter("vid"));
			String vname = request.getParameter("vname");
			String vgender = request.getParameter("vgender");
			String vaddress = request.getParameter("vaddress");

			VotersList list = new VotersList(vid, vname, vgender, vaddress);
			dao.update(list);

			List<VotersList> list1 = dao.viewAll();
			request.setAttribute("voterslist", list1);
			request.getRequestDispatcher("admincrudoperations.jsp").include(request, response);
		}

	}

}
