<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<h1>Admin Home Page</h1>
	<p align="right">
		Hi,
		<%=(String) session.getAttribute("uid")%>
		<a href="admincrudoperations">Admincrud Operations</a> <a
			href="signout">Logout</a>
	</p>
	<hr />

	<table border="3">
		<tr>
			<th colspan="4">VotersList</th>
		</tr>
		<tr>
			<th>VotersList id</th>
			<th>VotersList name</th>
			<th>VotersList gender</th>
			<th>VotersList address</th>

		</tr>
		<c:forEach items="${voterslist}" var="v">
			<tr style="text-align: center;">
				<td>${v.getVid()}</td>
				<td>${v.getVname()}</td>
				<td>${v.getVgender()}</td>
				<td>${v.getVaddress()}</td>

			</tr>

		</c:forEach>
	</table>

</body>
</html>