<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<h1>Admin CRUD Operations Page</h1>
	<p align="left">
		Hi,
		<%=(String) session.getAttribute("uid")%>
		<a href="home">Home</a> <a href="insert">Add VotersList</a> <a
			href="signout">Logout</a>
	</p>
	<hr />
	{msg}
	<table border="5">
		<tr>
			<th colspan="8">votersList</th>
		</tr>
		<tr>
			<th>VotersList id</th>
			<th>VotersList name</th>
			<th>VotersList gender</th>
			<th>VotersList address</th>
			<th>Update</th>
			<th>Delete</th>
		</tr>
		<c:forEach items="${votersList}" var="v">
			<tr style="text-align: left;">
			<td>${v.getVid()}</td>
				<td>${v.getVname()}</td>
				<td>${v.getVgender()}</td>
				<td>${v.getVaddress()}</td>
			<td><a style="text-decoration: none"
				href="update1?vid=${v.getVid()}&vname=${v.getVname()}&vgender=${v.getVgender()}&vaddress=${v.getVaddress()}">Update1</a></td>
				<td><a style="text-decoration: none"
					href="delete?vid=${v.getVid()}">Delete</a>
				</td>
			</tr>

		</c:forEach>
	</table>

</body>
</html>